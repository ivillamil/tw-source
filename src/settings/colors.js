export default {
    accent: '#D62C27',
    accentDark: '#AB1713',
    bg: '#3bd',
    bgDark: '#2ac',
    primary: '#F26100',
    primaryDark: '#BF4D00',
    twitter: '#3bd'
}