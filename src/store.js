import { createStore, applyMiddleware } from 'redux'
import { persistStore } from 'redux-persist'
import { AsyncStorage } from 'react-native'
import Reactotron from 'reactotron-react-native'
import thunkMiddleware from 'redux-thunk'
import reducers from './reducers'

const initialState = {
    session: {
        error: null,
        loading: false,
        ready: false,
        oauth: {}
    },

    tweets: {
        error: null,
        loading: false,
        items: []
    }
}

const store = (__DEV__ ? Reactotron.createStore : createStore)(reducers, initialState, applyMiddleware(thunkMiddleware))
persistStore(store, {
    storage: AsyncStorage,
    whitelist: ['session']
}).purge([])

export default store