import { connect } from 'react-redux'
import LoadingScreen from '../screens/LoadingScreen'

export default connect(({session}) => ({session}))(LoadingScreen)