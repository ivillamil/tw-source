import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { session, tweets } from '../actions'
import HomeScreen from '../screens/HomeScreen'

export default connect(
    ({session, tweets}) => ({
        session,
        loading: (session.loading || tweets.loading),
        tweets
    }),
    dispatch => bindActionCreators({ ...session, ...tweets }, dispatch)
)(HomeScreen)