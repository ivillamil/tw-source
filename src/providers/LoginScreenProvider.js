import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { session } from '../actions'
import LoginScreen from '../screens/LoginScreen'

export default connect(
    ({session}) => ({session, loading: session.loading}),
    dispatch => bindActionCreators({ ...session }, dispatch)
)(LoginScreen)