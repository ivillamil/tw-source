import CryptoJS from 'crypto-js'
import Base64 from 'crypto-js/enc-base64'

const keyValueMap = (collection) => (key) => `${encodeURIComponent(key)}=${encodeURIComponent(collection[key])}`

export function getSignatureForURL ({url, params, method, consumer_secret, consumer_key}) {
    if (!url || !consumer_key || !consumer_secret) {
        throw 'Parameters "url", "consumer_key" and "consumer_secret" are required'
    }
    if (params.oauth_token && !params.oauth_token_secret) {
        throw 'oauth_token_secret is required since you prived an oauth_token'
    }
    const _timeStamp = Math.round((new Date()).getTime() / 1000.0)
    const _nonceString = CryptoJS.SHA1(_timeStamp.toString())
    const _signingKey = encodeURIComponent(consumer_secret) + "&" + (
        (params && params.oauth_token_secret) 
            ? encodeURIComponent(params.oauth_token_secret) 
            : ''
    )
    const _method = method || 'post'
    const _defaultParams = {
        oauth_consumer_key: consumer_key,
        oauth_nonce: _nonceString,
        oauth_signature_method: 'HMAC-SHA1',
        oauth_timestamp: _timeStamp,
        oauth_version: '1.0'
    }
    const _params = { ..._defaultParams, ...params }
    const _parameterString = Object.keys(_params)
        .sort((a, b) => {
            const valueA = a.toLowerCase()
            const valueB = b.toLowerCase()
            if (valueA < valueB) return -1
            if (valueA > valueB) return 1
            return 0
        })
        .filter(key => (key !== 'oauth_token_secret'))
        .map(keyValueMap(_params))
        .join('&')
    const _signatureBaseString = `${_method.toUpperCase()}&${encodeURIComponent(url.split('?')[0])}&${encodeURIComponent(_parameterString)}`
    const signature = encodeURIComponent(Base64.stringify(CryptoJS.HmacSHA1(_signatureBaseString, _signingKey)))
    return {
        nonce: _nonceString,
        signature,
        signingKey: _signingKey,
        timestamp: _timeStamp
    }
}