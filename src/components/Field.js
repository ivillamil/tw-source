import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { 
    StyleSheet, 
    Text, 
    TextInput,
    TextInputProps,
    View 
} from 'react-native'
import { colors } from '../settings'

export default function Field ({ label, onChange, onSubmit, ...inputProps }) {
    return (
        <View style={styles.container}>
            <Text style={styles.label}>{label}:</Text>
            <TextInput  
                blurOnSubmit
                keyboardAppearance={'dark'}
                onChangeText={onChange}
                onEndEditing={onSubmit}
                placeholderTextColor={'#fffa'}
                {...inputProps}
                style={styles.input} 
            />
        </View>
    )
}

Field.propTypes = {
    ...TextInputProps,
    label: PropTypes.string
}

const styles = StyleSheet.create({
    container: {

    },

    input: {
        backgroundColor: '#fff3',
        borderColor: 'transparent',
        borderRadius: 6,
        color: colors.accent,
        fontSize: 16,
        height: 40,
        paddingHorizontal: 8
    },

    label: {
        color: '#fff',
        fontSize: 16,
        fontWeight: '300',
        marginBottom: 4
    }
})