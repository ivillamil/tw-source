import React from 'react'
import PropTypes from 'prop-types'
import { Keyboard, StyleSheet, Text } from 'react-native'

const Logo = ({size}) => (
    <Text 
        onPress={Keyboard.dismiss}
        style={[styles.title, styles[`title_${size}`]]}>
        <Text style={styles.light}>tw</Text>
        <Text style={styles.bold}>Zone</Text>
    </Text>
)

Logo.defaultProps = {
    size: 'large'
}

Logo.propTypes = {
    size: PropTypes.oneOf(['large', 'small'])
}

const styles = StyleSheet.create({
    bold: { 
        fontWeight: '700',
        letterSpacing: -3
    },

    light: { 
        color: '#fffa',
        fontWeight: '200',
        letterSpacing: -2
    },

    title: {
        color: '#fff'
    },

    title_large: {
        fontSize: 64
    },

    title_small: {
        fontSize: 24
    }
})

export default Logo