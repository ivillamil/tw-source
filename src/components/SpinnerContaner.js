import React from 'react'
import { ActivityIndicator, StyleSheet, View } from 'react-native'

export default function SpinnerContainer (WrappedComponent) {
    function Enhaced({loading, ...props}) {
        return (
            <View 
                style={styles.container}>
                <WrappedComponent {...props} />
                {loading && (
                    <View 
                        pointerEvents={'none'}
                        style={styles.spinnerContainer}>
                        <ActivityIndicator 
                            animating
                            color={'#fff'}
                            size={'large'}
                        />
                    </View>
                )}
            </View>
        )
    }

    if (WrappedComponent.navigationOptions) {
        Enhaced.navigationOptions = WrappedComponent.navigationOptions
    }

    return Enhaced
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    spinnerContainer: {
        alignItems: 'center',
        backgroundColor: '#0001',
        bottom: 0,
        justifyContent: 'center',
        left: 0,
        position: 'absolute',
        right: 0,
        top: 0
    }
})