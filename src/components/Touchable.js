import React from 'react'
import {
    Platform,
    TouchableOpacity,
    TouchableNativeFeedback,
    View
} from 'react-native'

export default function Touchable ({children, style, ...props}) {
    return Platform.select({
        android: (
            <TouchableNativeFeedback {...props}>
                <View style={style}>{children}</View>
            </TouchableNativeFeedback>
        ),
        ios: (
            <TouchableOpacity {...props} style={style}>
                {children}
            </TouchableOpacity>
        )
    })
}