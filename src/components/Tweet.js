import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { Image, StyleSheet, Text, View } from 'react-native'
import { colors } from '../settings'

export default function Tweet ({tweet}) {
    const _date = new Date(tweet.created_at)
    return (
        <View style={styles.container}>
            <View style={styles.row}>
                <Image 
                    source={{uri: tweet.user.profile_image_url_https}} 
                    style={styles.profileImage}
                />
                <View style={{width: 8}} />
                <View style={styles.content}>
                    <Text style={styles.userName}>{tweet.user.name}</Text>
                    <Text style={styles.date}>{moment(_date).fromNow()}</Text>
                </View>
            </View>
            <View style={{height: 8}} />
            <View style={styles.tweetContent}>
                <Text style={styles.tweet}>{tweet.text}</Text>
            </View>
        </View>
    )
}

Tweet.propTypes = {
    tweet: PropTypes.object
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        borderRadius: 4,
        elevation: 2
    },

    content: {
        flex: 1
    },

    date: {
        fontSize: 13,
        opacity: 0.8
    },

    profileImage: {
        borderRadius: 30,
        height: 40,
        width: 40
    },

    row: {
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal: 16,
        marginTop: 16
    },

    tweet: {
        fontSize: 16
    },

    tweetContent: {
        backgroundColor: '#fafafa',
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
        padding: 16
    },

    userName: {
        color: colors.primaryDark,
        fontSize: 16,
        fontWeight: '600'
    }
})