import React from 'react'
import { StyleSheet, Text } from 'react-native'
import { Touchable } from '../components'
import { colors } from '../settings'

export default function CTAButton ({ label, onPress, style }) {
    return (
        <Touchable
            activeOpacity={0.8}
            onPress={onPress}
            style={[styles.button, style]}>
            <Text style={styles.buttonLabel}>{label}</Text>
        </Touchable>
    )
}

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        backgroundColor: colors.accent,
        borderRadius: 6,
        elevation: 3,
        height: 56,
        justifyContent: 'center'
    },

    buttonLabel: {
        color: '#fff',
        fontSize: 18,
        fontWeight: '500'
    }
})