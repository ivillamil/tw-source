import axios from 'axios'
import Config from 'react-native-config'
import { OAuthUtils } from '../services'

export const REQUEST_ACCESS_TOKEN_ERROR = 'REQUEST_ACCESS_TOKEN_ERROR'
export const REQUEST_ACCESS_TOKEN_REQUEST = 'REQUEST_ACCESS_TOKEN_REQUEST'
export const REQUEST_ACCESS_TOKEN_SUCCESS = 'REQUEST_ACCESS_TOKEN_SUCCESS'

export const REQUEST_TOKEN_ERROR = 'REQUEST_TOKEN_ERROR'
export const REQUEST_TOKEN_REQUEST = 'REQUEST_TOKEN_REQUEST'
export const REQUEST_TOKEN_SUCCESS = 'REQUEST_TOKEN_SUCCESS'

export const SET_OAUTH = 'SET_OAUTH'

export const SIGN_OUT_ERROR = 'SIGN_OUT_ERROR'
export const SIGN_OUT_REQUEST = 'SIGN_OUT_REQUEST'
export const SIGN_OUT_SUCCESS = 'SIGN_OUT_SUCCESS'

export function requestAccessToken ({oauth_token, oauth_token_secret, oauth_verifier}) {
    return async function (dispatch) {
        dispatch(requestAccessTokenRequest())
        try {
            const url = Config.ACCESS_TOKEN_URL
            const params = {oauth_token, oauth_verifier}
            if (oauth_token_secret) params.oauth_token_secret = oauth_token_secret
            const {nonce, signature, timestamp} = OAuthUtils.getSignatureForURL({
                url,
                params,
                method: 'post',
                consumer_key: Config.CONSUMER_KEY,
                consumer_secret: Config.CONSUMER_SECRET
            })
            const authHeaders = [
                `OAuth oauth_consumer_key="${encodeURIComponent(Config.CONSUMER_KEY)}"`,
                `oauth_nonce="${nonce}"`,
                `oauth_signature="${signature}"`,
                `oauth_signature_method="HMAC-SHA1"`,
                `oauth_timestamp="${timestamp}"`,
                `oauth_token="${encodeURIComponent(oauth_token)}"`,
                `oauth_verifier="${encodeURIComponent(oauth_verifier)}"`,
                'oauth_version="1.0"'
            ].join(', ')
            const response = await axios({
                method: 'post',
                url: '/oauth/access_token',
                baseURL: 'https://api.twitter.com/',
                headers: {'authorization': authHeaders}
            })
            if (response && response.data) {
                const [oauth_token, oauth_token_secret, user_id, screen_name] = response.data.split('&').map(item => {
                    const [key, value] = item.split('=')
                    return value
                })
                dispatch(requestAccessTokenSuccess({
                    oauth_token,
                    oauth_token_secret,
                    screen_name,
                    user_id
                }))
            }
        } catch (error) {
            dispatch(requestAccessTokenError(error))
        }
    }
}

function requestAccessTokenError (error) {
    return {
        type: REQUEST_ACCESS_TOKEN_ERROR,
        payload: {error}
    }
}

function requestAccessTokenRequest () {
    return {
        type: REQUEST_ACCESS_TOKEN_REQUEST
    }
}

function requestAccessTokenSuccess (resp) {
    return {
        type: REQUEST_ACCESS_TOKEN_SUCCESS,
        payload: {...resp}
    }
}

export function requestToken () {
    return async function (dispatch) {
        dispatch(requestTokenRequest())
        try {
            const url = Config.REQUEST_TOKEN_URL
            const oauth_callback = Config.SCHEME + 'auth-callback'
            const {nonce, signature, timestamp} = OAuthUtils.getSignatureForURL({
                url,
                params: {oauth_callback},
                method: 'post',
                consumer_key: Config.CONSUMER_KEY,
                consumer_secret: Config.CONSUMER_SECRET
            })
            const authHeaders = [
                `OAuth oauth_callback="${encodeURIComponent(oauth_callback)}"`,
                `oauth_consumer_key="${encodeURIComponent(Config.CONSUMER_KEY)}"`,
                `oauth_nonce="${nonce}"`,
                `oauth_signature="${signature}"`,
                `oauth_signature_method="HMAC-SHA1"`,
                `oauth_timestamp="${timestamp}"`,
                'oauth_version="1.0"'
            ].join(', ')
            const response = await axios({
                method: 'post',
                url: '/oauth/request_token',
                baseURL: 'https://api.twitter.com/',
                headers: {'authorization': authHeaders}
            })
            if (response) {
                if (response.data) {
                    const [oauth_token, oauth_token_secret, oauth_callback_confirmed] = response.data.split('&').map(item => {
                        const [key, value] = item.split('=')
                        return value
                    })
                    dispatch(requestTokenSuccess({
                        oauth_callback_confirmed,
                        oauth_token,
                        oauth_token_secret
                    }))
                }
            }
        } catch (error) {
            dispatch(requestTokenError(error))
        }
    }
}

function requestTokenError (error) {
    return {
        type: REQUEST_TOKEN_ERROR,
        payload: {error}
    }
}

function requestTokenRequest () {
    return {
        type: REQUEST_TOKEN_REQUEST
    }
}

function requestTokenSuccess (resp) {
    return {
        type: REQUEST_TOKEN_SUCCESS,
        payload: {...resp}
    }
}

export function setOauth (props) {
    return {
        type: SET_OAUTH,
        payload: {
            ...props
        }
    }
}

export function signOut () {
    return function (dispatch) {
        dispatch(signOutRequest())
        setTimeout(() => {
            dispatch(signOutSuccess())
        }, 1000)
    }
}

function signOutError (error) {
    return {
        type: SIGN_OUT_ERROR,
        payload: {error}
    }
}

function signOutRequest () {
    return {
        type: SIGN_OUT_REQUEST
    }
}

function signOutSuccess () {
    return {
        type: SIGN_OUT_SUCCESS
    }
}