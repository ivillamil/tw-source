import axios from 'axios'
import Config from 'react-native-config'
import { OAuthUtils } from '../services'

export const FETCH_ERROR = 'FETCH_ERROR'
export const FETCH_REQUEST = 'FETCH_REQUEST'
export const FETCH_SUCCESS = 'FETCH_SUCCESS'

export function fetch ({oauth_token, oauth_token_secret, queryParams}) {
    return async function (dispatch) {
        dispatch(fetchRequest())
        try {
            const query = queryParams || '?q=autozone'
            const endPoint = '/1.1/search/tweets.json'
            const url = Config.BASE_API_URL + endPoint
            const {nonce, signature, timestamp} = OAuthUtils.getSignatureForURL({
                url,
                method: 'get',
                consumer_key: Config.CONSUMER_KEY,
                consumer_secret: Config.CONSUMER_SECRET,
                params: {
                    oauth_token,
                    oauth_token_secret,
                    ...(
                        query.replace('?', '').split('&').reduce((obj, item) => {
                            const [key, value] = item.split('=')
                            return {...obj, [key]: value}
                        }, {})
                    )
                }
            })
            const authHeaders = [
                `OAuth oauth_consumer_key="${encodeURIComponent(Config.CONSUMER_KEY)}"`,
                `oauth_nonce="${nonce}"`,
                `oauth_signature="${signature}"`,
                `oauth_signature_method="HMAC-SHA1"`,
                `oauth_timestamp="${timestamp}"`,
                `oauth_token="${encodeURIComponent(oauth_token)}"`,
                `oauth_version="1.0"`
            ].join(', ')
            const response = await axios({
                method: 'get',
                url: '/1.1/search/tweets.json' + query,
                baseURL: Config.BASE_API_URL,
                headers: {'authorization': authHeaders}
            })
            if (response && response.data && response.status === 200) {
                dispatch(fetchSuccess({
                    tweets: response.data.statuses,
                    metadata: response.data.search_metadata,
                    append: (undefined !== queryParams)
                }))
            }
        } catch (error) {
            dispatch(fetchError(error))
        }
    }
}

function fetchError (error) {
    return {
        type: FETCH_ERROR,
        payload: {error}
    }
}

function fetchRequest () {
    return {
        type: FETCH_REQUEST
    }
}

function fetchSuccess ({tweets, metadata, append}) {
    return {
        type: FETCH_SUCCESS,
        payload: {append, metadata, tweets}
    }
}