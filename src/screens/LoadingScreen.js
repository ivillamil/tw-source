import React, { useEffect } from 'react'
import { ActivityIndicator, StatusBar, StyleSheet, View } from 'react-native'
import { Logo } from '../components'
import { colors } from '../settings'

export default function LoadingScreen (props) {

    useEffect(() => {
        const {oauth} = props.session
        if (props.session.ready)
            props.navigation.navigate((oauth.oauth_token && oauth.user_id) ? 'App' : 'Auth')
    }, [props.session.ready])

    return (
        <View style={styles.container}>
            <StatusBar 
                animated
                backgroundColor={colors.primaryDark}
                barStyle={'light-content'}
            />
            <Logo />
            <View style={{height: 20}} />
            <ActivityIndicator
                animating
                color={'#fff'}
                size={'large'} 
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: colors.primary,
        flex: 1,
        justifyContent: 'center'
    }
})