import React, { useEffect, useState } from 'react'
import { Alert, Button, FlatList, RefreshControl, StatusBar, StyleSheet, Text, View } from 'react-native'
import { Logo, SpinnerContainer, Tweet } from '../components'
import { colors } from '../settings'

function HomeScreen (props) {
    const [currentTime, setCurrentTime] = useState((new Date()).getTime())
    const [refreshing, setRefreshing] = useState(false)
    const [tweets, setTweets] = useState([])
    let interval = null

    const _renderSeparator = () => <View style={{height: 8}} />
    const _renderTweet = ({item}) => <Tweet tweet={item} />

    function _fetchTweets () {
        const {oauth: {oauth_token, oauth_token_secret}} = props.session
        props.fetch({oauth_token, oauth_token_secret})
    }

    function _handleRefresh () {
        if (refreshing) return
        setRefreshing(true)
        _fetchTweets()
    }

    function _loadMoreTweets () {
        const {
            loading,
            session: {oauth: {oauth_token, oauth_token_secret}}, 
            tweets: {metadata: {next_results}}
        } = props
        if (loading || refreshing) return
        props.fetch({oauth_token, oauth_token_secret, queryParams: next_results})
    }

    function _signOut () {
        Alert.alert(
            'Signing Out',
            'Are you sure you want to close your session?',
            [
                {text: 'Cancel'},
                {
                    text: 'Sign Out',
                    onPress: props.signOut
                }
            ]
        )
    }

    /**
     * Setup header button and fetches the tweets.
     */
    useEffect(() => {
        if (!props.navigation.getParam('signOut')) {
            props.navigation.setParams({signOut: _signOut})
            _fetchTweets()
            interval = setInterval(() => {
                setCurrentTime((new Date()).getTime())
            }, (1000 * 30))
        }
    })

    /**
     * Listen to changes to the oauth state and navigates
     * to the authentication stack when the session ends.
     */
    useEffect(() => {
        const {oauth} = props.session
        if (!oauth.oauth_token && !oauth.user_id) {
            props.navigation.navigate('Auth')
        }
    }, [props.session.oauth])

    /**
     * Listen to changes on the tweets collection and
     * asigned every new change to the collection in
     * the component state.
     */
    useEffect(() => {
        const {tweets} = props.tweets
        setTweets(tweets)
        setRefreshing(false)
    }, [props.tweets.tweets])

    return (
        <View style={styles.container}>
            <StatusBar 
                animated
                backgroundColor={colors.primaryDark}
                barStyle={'light-content'}
            />
            <FlatList 
                contentContainerStyle={styles.content}
                data={tweets}
                extraData={currentTime}
                keyExtractor={tweet => tweet.id_str}
                ItemSeparatorComponent={_renderSeparator}
                onEndReached={_loadMoreTweets}
                onEndReachedThreshold={0.9}
                renderItem={_renderTweet}
                refreshControl={(
                    <RefreshControl 
                        refreshing={refreshing}
                        onRefresh={_handleRefresh}
                    />
                )}
                style={{flex: 1}}
            />
        </View>
    )
}

/**
 * Navigation Settings
 */
HomeScreen.navigationOptions = ({navigation}) => {
    return {
        title: 'TwZone',
        headerRight: (
            <View style={{paddingRight: 16}}>
                <Button 
                    color={colors.accent}
                    onPress={navigation.getParam('signOut')}
                    title="Sign Out" />
            </View>
        ),
        headerTitle: (
            <View style={{paddingLeft: 16}}><Logo size={'small'} /></View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    content: {
        padding: 8
    }
})

export default SpinnerContainer(HomeScreen)