import React, { useEffect, useState } from 'react'
import { Linking, StatusBar, StyleSheet, Text, View } from 'react-native'
import Config from 'react-native-config'
import { CTAButton, Logo, SpinnerContainer } from '../components'
import { colors } from '../settings'

function LoginScreen (props) {
    const [submitting, setSubmitting] = useState(false)

    function _handleTwitterCallback ({url}) {
        const [scheme, uri] = url.split('://')
        const [host, params] = uri.split('?')
        if (host === 'auth-callback') {
            const [oauth_token, oauth_verifier] = params.split('&').map(item => item.split('=')[1])
            props.setOauth({
                oauth_token,
                oauth_verifier
            })
        }
    }

    function _handleTwitterSubmit () {
        setSubmitting(true)
        props.requestToken()
    }

    useEffect(() => {
        Linking.addEventListener('url', _handleTwitterCallback)
        return () => Linking.removeEventListener('url', _handleTwitterCallback)
    })

    useEffect(() => {
        const {oauth} = props.session
        if (submitting) {
            if (oauth.oauth_callback_confirmed && !oauth.oauth_verifier) {
                const url = Config.AUTHORIZE_URL + '?oauth_token=' + oauth.oauth_token
                if (Linking.canOpenURL(url)) {
                    Linking.openURL(url)
                }
            } else if (oauth.oauth_callback_confirmed && oauth.oauth_verifier) {
                if (!oauth.user_id) {
                    props.requestAccessToken(oauth)
                } else {
                    props.navigation.navigate('App')
                    setSubmitting(false)
                }
            }
        }
        
    }, [props.session.oauth])

    return (
        <View style={styles.container}>
            <StatusBar 
                animated
                backgroundColor={colors.primaryDark}
                barStyle={'light-content'}
            />
            <View style={styles.logoWrapper}><Logo /></View>
            <View style={styles.content}>
                <CTAButton 
                    label={'Sign In with Twitter'}
                    onPress={_handleTwitterSubmit}
                    style={{backgroundColor: colors.accentDark}}
                />
            </View>
        </View>
    )
}

LoginScreen.navigationOptions = {
    title: 'Sign In'
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.primary,
        flex: 1,
        justifyContent: 'center'
    },

    content: {
        marginHorizontal: 32
    },

    logoWrapper: {
        alignItems: 'center',
        marginBottom: 48
    }
})

export default SpinnerContainer(LoginScreen)