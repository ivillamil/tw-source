import { REHYDRATE } from 'redux-persist/constants'
import { session } from '../actions'

export default function (state = {}, {payload, type}) {
    switch (type) {
        case session.REQUEST_ACCESS_TOKEN_ERROR:
        case session.REQUEST_TOKEN_ERROR:
        case session.SIGN_OUT_ERROR:
            return {
                ...state,
                error: payload.error,
                loading: false
            }

        case session.REQUEST_ACCESS_TOKEN_REQUEST:
        case session.REQUEST_TOKEN_REQUEST:
        case session.SIGN_OUT_REQUEST:
            return {
                ...state,
                error: null,
                loading: true
            }

        case session.REQUEST_ACCESS_TOKEN_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
                oauth: {
                    ...state.oauth,
                    ...payload
                }
            }

        case session.REQUEST_TOKEN_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
                oauth: {
                    ...state.oauth,
                    ...payload
                }
            }

        case session.SET_OAUTH:
            return {
                ...state,
                oauth: {
                    ...state.oauth,
                    ...payload
                }
            }

        case session.SIGN_OUT_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
                oauth: {}
            }

        case REHYDRATE:
            const incoming = payload.session
            if (incoming) {
                return {
                    ...state,
                    ...incoming,
                    loading: false,
                    ready: true
                }
            } else {
                return {
                    ...state,
                    ready: true
                }
            }

        default: 
            return state
    }
}