import { tweets } from '../actions'

export default function (state = {}, {payload, type}) {
    switch (type) {
        case tweets.FETCH_ERROR:
            return {
                ...state,
                error: payload.error,
                loading: false
            }

        case tweets.FETCH_REQUEST:
            return {
                ...state,
                error: null,
                loading: true
            }

        case tweets.FETCH_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
                tweets: (payload.append) 
                    ? [ ...state.tweets, ...payload.tweets ]
                    : payload.tweets,
                metadata: payload.metadata
            }
        
        default:
            return state
    }
}