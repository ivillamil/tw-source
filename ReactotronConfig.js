import Reactotron from 'reactotron-react-native'
import { reactotronRedux } from 'reactotron-redux'

Reactotron
    .configure({
        host: '10.0.2.2',
        name: "twZone"
    })
    .use(reactotronRedux())
    .useReactNative()
    .connect()

console.tron = __DEV__ ? Reactotron : console