import React from 'react'
import { createStackNavigator, createSwitchNavigator } from 'react-navigation'
import { HomeScreen, LoadingScreen, LoginScreen, TwitterAuthScreen } from './src/screens'
import { colors } from './src/settings'

const navigationOptions = {
  headerStyle: {
    backgroundColor: colors.primary
  },
  headerTintColor: '#fff'
}

const AppStack = createStackNavigator({Home: HomeScreen}, {navigationOptions})
const AuthStack = createStackNavigator({
  Login: LoginScreen,
  TwitterAuth: TwitterAuthScreen,
}, {headerMode: 'none', navigationOptions})

export default createSwitchNavigator(
  {
    AuthLoading: LoadingScreen,
    App: AppStack,
    Auth: AuthStack
  },
  {
    initialRouteName: 'AuthLoading'
  }
)